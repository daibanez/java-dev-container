
plugins {
    // Apply the application plugin to add support for building a CLI application.
    application
}

repositories {
    // Use jcenter for resolving dependencies.
    // You can declare any Maven/Ivy/file repository here.
    jcenter()
}

dependencies {
    // Use the Kotlin test library.
    testImplementation("junit:junit:4.12")
}

application {
    // Define the main class for the application.
    mainClassName = "dai.demo.App"
}
